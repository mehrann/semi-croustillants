#!/usr/bin/env python
# coding: utf-8

# In[ ]:


## Competition Kaggle
##
## Participants:
## Pierre-Olivier L'Espérance
## Yi Jing Wang
##
## Equipe:
## Les semi-croustillants

## Run all to 
##  - Compute predictions
##  - Create file prediction.csv

##
## Imports and download
##

import numpy as np
import string
import nltk
import nltk.corpus
import csv
from nltk.tokenize import word_tokenize
from nltk.probability import FreqDist
from nltk.corpus import stopwords
nltk.download('stopwords')
nltk.download('wordnet')
nltk.download('punkt')
from nltk.stem import WordNetLemmatizer
lemmatizer = WordNetLemmatizer() 
from nltk.stem.snowball import SnowballStemmer
stemmer= SnowballStemmer("english")
from nltk.stem import PorterStemmer
pst = PorterStemmer()


# In[ ]:


##
## Start - methods we need
##
def loadTrainData():
    train_data = np.load('data_train.pkl',allow_pickle=True)
    traindata = np.asarray(train_data)
    traindata = np.transpose(traindata)
    test_data = np.load('data_test.pkl',allow_pickle=True)
    testdata = np.asarray(test_data)
    return traindata, testdata


def subjectUnique(data):
    sujet = data[:,1]
    sujetset = np.unique(sujet)
    print(sujetset)
    sujetsetReduit = sujetset.copy()
    for i in range(len(sujetset)):
        sujetsetReduit[i] = sujetset[i][:3]
    return sujetset,sujetsetReduit


def getSentenceTable(data,ensSujet):
    phrase = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
    sujetsetReduit = list(ensSujet)
    for i,x in enumerate(data):
        indice = (sujetsetReduit.index(x[1][:3]))
        phrase[indice] += x[0]
    for i in range(len(phrase)):
        phrase[i] = (''.join(phrase[i]))
    return phrase


def cleanSentence(ssentence):
    ssentence = ssentence.replace('_',' ')
    for c in ['0','1','2','3','4','5','6','7','8','9','www','.com','https','http', '.', '/' , '%', '#', '@', '$', '?', '&', '*', '(', ')', '-', '_', '=']:
        ssentence = ssentence.replace(c,' ')
    return ssentence


def cleanTrainData(ArraySentence):
    arraycleaned = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
    for i in range(len(ArraySentence)):
        sentence = ArraySentence[i]
        sentence = cleanSentence(sentence)
        arraycleaned[i] = sentence
    return arraycleaned


#Remove duplicate character from a string
def removeDuplicate(mot):
    mots = []
    mots += [mot[0]]
    mot = mot[1:]
    i = 0
    for ch in mot:
        if mots[i]!= ch :
            mots += [ch]
            i +=1
    return ''.join(mots)


def tokenise(sentence):
    tokenizer = nltk.RegexpTokenizer(r'\w+')
    tokens = tokenizer.tokenize(sentence.lower()) 
    stop = stopwords.words('english')
    cleantext = []
    #cleantext += [stemmer.stem(w) for w in tokens if len(stemmer.stem(w))>1 and not w in punct_stopwords]
    #cleantext += [pst.stem(w) for w in tokens if len(w)>1 and not w in punct_stopwords]
    for w in tokens:
        motts = removeDuplicate(w) #Remove duplicate character from a string
        #motts = lemmatizer.lemmatize(motts)
        motts = stemmer.stem(motts)
        if len(motts) > 0 and not motts in stop:
            cleantext += [motts]
        
    return cleantext

def cleaning(sentence):
    tokenizer = nltk.RegexpTokenizer(r'\w+')
    tokens = tokenizer.tokenize(sentence.lower()) 
    stop = stopwords.words('english')
    punct_stopwords = stop
    cleantext = []
    cleantext += [lemmatizer.lemmatize(w) for w in tokens if len(w)>1 and w.isalpha() and not w in punct_stopwords]

    return cleantext



def frequenceCounter(sentences):
    return nltk.FreqDist(sentences)


def analysedata(arrraySentence):
    tokenCombiner = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
    frequence = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
    for i in range(len(arrraySentence)):
        tokens = tokenise(arrraySentence[i])
        tokenCombiner[i] = tokens
        frequence[i] = fq = frequenceCounter(tokens)
    return tokenCombiner,frequence


def extractVoc(minFreq,frequences):
    mots = []
    for i in range(20):
        mots += [k for k,v in frequences[i].items() if int(v) >= minFreq]
    print(len(mots))
    motUnique = np.unique(mots)
    return motUnique


def maticeFrequece(Voc,frequences):
    freqMatrice = np.zeros((20,len(Voc)))
    print(freqMatrice.shape)
    for i in range(20):
        for j,x in enumerate(Voc):
            fr = frequences[i].get(x)
            if fr is None:
                freqMatrice[i,j] = 0
            else:
                freqMatrice[i,j] = fr
    return freqMatrice


def nettoyerVoc(freqMatrice,vocabulaire,minProb):
    sumex = np.sum(freqMatrice,axis=0)    #Additionner les frequence pour chaque mots de tout les sujets
    freqMatriceSum = freqMatrice/sumex  #Diviser chaque colonne de frequence par leur somme, cela donne proportion de chaque mots
    print(freqMatriceSum[:,0])
    propmax = np.amax(freqMatriceSum, axis=0)  #Cherche le proportion maximal pour chaque mots
    #print(propmax[indd])
    mindemax = []
    for i in range(len(propmax)):
        if propmax[i] < minProb:        #Checher les indices dont les prop max sont <0.08 
            mindemax += [i]
    print(len(mindemax))
    print(vocabulaire[mindemax])
    print(propmax[mindemax])
    vocExtract = np.delete(vocabulaire, mindemax)   #Enlever les mots dont les prop max sont <0.08
    return vocExtract


def extractTest(datatest,vocab):
    tokenTest = []
    frequenceTest = []
    for textt in datatest:
        textt = cleanSentence(textt)
        tokenn = tokenise(str(textt))
        cleantoken = [w for w in tokenn if w in vocab]
        tokenTest += [cleantoken]
        frequenceTest += [frequenceCounter(cleantoken)]
    return tokenTest,frequenceTest


def Lapl(islog, tokenTest,vocab,frrequence,mmatriceFreq):
    probTableLapl = np.zeros((len(tokenTest),20))
    sujetProbableLapl = np.zeros(len(tokenTest))
    propSujet = np.log(3500/70000)
    if not islog:
        propSujet = 3500/70000
    nbVoc = len(vocab)
    sumMot = np.sum(mmatriceFreq,axis=1)
    testtableLapl = np.zeros(len(tokenTest[0]))
    for extest in range(probTableLapl.shape[0]):
        for exdata in range(20):
            testtableLapl = np.zeros(len(tokenTest[extest]))
            for i,x in enumerate(tokenTest[extest]):
                datafr = frrequence[exdata].get(x)
                if datafr is None:
                    datafr = 0
                datafr += 1
                nbMot = sumMot[exdata]
                nbMot += nbVoc
                testtableLapl[i] = np.log((datafr)/(nbMot))
                if not islog:
                    testtableLapl[i] = (datafr)/(nbMot)
            probProduct = np.sum(testtableLapl)
            if not islog:
                probProduct = np.prod(testtableLapl)
            probTableLapl[extest,exdata] = probProduct+propSujet
            if not islog:
                probTableLapl[extest,exdata] = probProduct*propSujet
    return probTableLapl


def sujetPred(sujetProb,ensSujet):
    sujetPred = []
    for x in sujetProb:
        sujetPred += [ensSujet[x]]
    return sujetPred


def writeCsvResult(fileName,prediction):
    csvTitle = [['Id', 'Category']]
    csvpred = enumerate(prediction)
    with open(fileName, 'w', newline='',encoding='utf-8') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(csvTitle)
        writer.writerows(csvpred)
    csvFile.close()

    
def readCsv(fileName):
    tableBackup = []
    file = open(fileName, 'r',encoding='utf-8')
    reader = csv.DictReader(file)
    for row in reader:
        tableBackup += [row['Category']]
    return tableBackup


def writeDifference(fileName,newPred,oldPred,tokenTest,testdata):
    file = open(fileName, 'w',encoding='utf-8')
    for i in range(len(newPred)):
        if newPred[i] != oldPred[i]:
            file.write("*********************************************************************************************\n")
            file.write("*********************************************************************************************\n")
            file.write("id: "+str(i)+'\t\t')
            file.write("newPred: "+ newPred[i] + '\t\t')
            file.write("oldPred: "+ oldPred[i] + '\n')
            file.write("tokenTest: "+'|'+ ' | '.join(tokenTest[i]) +' |'+ '\n')
            file.write("sentence: "+testdata[i]+'\n\n\n')
    file.close()

    
def testDifference(predNew,predOld):
    testdiff = []
    for i in range(len(predNew)):
        if predNew[i] != predOld[i]:
            testdiff+= [(i,predNew[i],predOld[i])]
    return testdiff
##
## End of methods we need
##


# In[ ]:


##
## All the commands lines
##

# All the commands we need for traindata
traindata, testdata = loadTrainData()
sujets,sujetsReduit = subjectUnique(traindata)
dataTexts = getSentenceTable(traindata,sujetsReduit)
dataTextClean = cleanTrainData(dataTexts)
tokensData,freqData = analysedata(dataTextClean)
vocab = extractVoc(1,freqData)
matriceDataFreq = maticeFrequece(vocab,freqData)
minProp = 0
cleanVocab = nettoyerVoc(matriceDataFreq,vocab,minProp)
cleanmatriceDataFreq = maticeFrequece(cleanVocab,freqData)

# All the commands we need for testdata
tokensTest,frequenceTest = extractTest(testdata,cleanVocab)

# Compute Probablity
prob_lapl = Lapl(True,tokensTest,cleanVocab,freqData,cleanmatriceDataFreq)
predLog = sujetPred(np.argmax(prob_lapl, sujets)

#Write file
writeCsvResult('predictionProb_to_return.csv', predLog)


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




